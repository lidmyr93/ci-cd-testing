beforeEach(() => {
  cy.clearLocalStorage();
});

describe("Login test", () => {
  it("Visits my localhost", () => {
    cy.visit("/");

    cy.get('input[name="email"]')
      .type("user.name@domain.com")
      .should("have.value", "user.name@domain.com");

    cy.get('input[name="password"]')
      .type("Mypass99")
      .should("have.value", "Mypass99");

    cy.get('input[type="submit"]').click();

    cy.get(".user").should("contain", "user.name@domain.com");
  });
});
