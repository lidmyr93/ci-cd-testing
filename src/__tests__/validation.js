import {
  validateEmail,
  validatePassword,
  validateLogin
} from "../utils/validation";

test("passes on valid email", () => {
  expect(validateEmail("hejsan@sda.com")).toBeTruthy();
});

test("fails on invalid email", () => {
  expect(validateEmail("dsadsadas")).not.toBeTruthy();
});

test("validates password: 8 chars, 1 uppercase, 1 digit", () => {
  expect(validatePassword("abcdefgH1")).toBeTruthy();
});

test("invalid password: missing digit", () => {
  expect(validatePassword("abcdefgH")).not.toBeTruthy();
});

test("invalid password: missing 1 uppercase", () => {
  expect(validatePassword("abcdefgh1")).not.toBeTruthy();
});

test("validateLogin to return true if correct", () => {
  expect(validateLogin("hejsan@sda.com", "abcdefgH1")).toBeTruthy();
});
test("validateLogin to return false if correct", () => {
  expect(validateLogin("dsadsadas", "dsadsadas")).not.toBeTruthy();
});
