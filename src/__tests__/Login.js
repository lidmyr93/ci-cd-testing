import React from "react";
import { mount } from "enzyme";
import Login from "../components/Login";
import { unstable_batchedUpdates } from "react-dom";

test("simulate login failed", () => {
  const fakeLogin = jest.fn();
  const wrapper = mount(<Login loginSuccessful={fakeLogin} />);
  wrapper.find("form").simulate("submit");

  expect(wrapper.state().message.type).toBe("ERROR");
  expect(wrapper.state().message.body).toBe("Wrong password or email");
});

test("simulate login success", () => {
  const fakeLogin = jest.fn();
  const wrapper = mount(<Login loginSuccessful={fakeLogin} />);
  wrapper.setState({ email: "hejsan@asd.com", password: "abcdefgH1" });
  wrapper.find("form").simulate("submit");
  expect(wrapper.state().message.type).toBe("SUCCESS");
  expect(wrapper.state().message.body).toBe("Logged in");
});

test("handle change to set state correctly", () => {
  const fakeLogin = jest.fn();
  const wrapper = mount(<Login loginSuccessful={fakeLogin} />);
  const fakeTarget = {
    name: "email",
    value: "hejsan@asd.com"
  };

  wrapper.instance().handleChange({ target: fakeTarget });
  expect(wrapper.state().email).toEqual("hejsan@asd.com");
});

test("Input change should call handlechange", () => {
  const fakeLogin = jest.fn();
  const wrapper = mount(<Login loginSuccessful={fakeLogin} />);
  const emailEvent = { target: { name: "email", value: "zero@cool.gg" } };
  const passwordEvent = { target: { name: "password", value: "ValidateMe99" } };
  wrapper.find('input[name="email"]').simulate("change", emailEvent);
  wrapper.find('input[name="password"]').simulate("change", passwordEvent);
  wrapper.find('[data-test="form"]').simulate("submit");
  expect(wrapper.find(".success").exists()).toBeTruthy();
});
