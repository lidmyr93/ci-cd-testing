import {
  saveUserToLocalStorage,
  getUserFromLocalStorage,
  removeUserFromLocalStorage
} from "../utils/localStorage";

beforeEach(() => {
  localStorage.clear();
});

afterEach(() => {
  localStorage.clear();
});

test("saveUserToLocalStorage works", () => {
  saveUserToLocalStorage("hejsan@sda.com");
  expect(localStorage.getItem("user")).toEqual("hejsan@sda.com");
});
test("should get the user from localStorage", () => {
  saveUserToLocalStorage("hejsan@sda.com");
  localStorage.setItem("user", "hejsan@sda.com");
  expect(getUserFromLocalStorage()).toEqual("hejsan@sda.com");
});

test("should get empty user from localStorage", () => {
  expect(getUserFromLocalStorage()).toBe("");
});
test("removing user from localstorage", () => {
  localStorage.setItem("user", "hejsan@sda.com");
  expect(localStorage.getItem("user")).toBeTruthy();
  removeUserFromLocalStorage();
  expect(localStorage.getItem("user")).toEqual(null);
});
